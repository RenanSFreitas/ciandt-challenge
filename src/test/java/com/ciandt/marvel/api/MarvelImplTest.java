package com.ciandt.marvel.api;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;

import javax.ws.rs.core.Response.Status;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ciandt.marvel.api.client.MarvelClient;
import com.ciandt.marvel.api.model.MarvelDataContainer;
import com.ciandt.marvel.api.model.MarvelResponse;
import com.ciandt.marvel.api.model.Price;
import com.ciandt.marvel.api.model.character.Character;
import com.ciandt.marvel.api.model.character.CharacterDataContainer;
import com.ciandt.marvel.api.model.character.CharacterResponse;
import com.ciandt.marvel.api.model.comic.Comic;
import com.ciandt.marvel.api.model.comic.ComicDataContainer;
import com.ciandt.marvel.api.model.comic.ComicResponse;
import com.ciandt.marvel.api.parameter.comic.ComicFormat;
import com.ciandt.marvel.api.parameter.comic.ComicParameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MarvelResponse.class, MarvelDataContainer.class })
public class MarvelImplTest {

	@InjectMocks
	private MarvelImpl subject;

	@Mock
	private MarvelClient client;

	@Mock
	private CharacterResponse characterResponse;

	@Mock
	private CharacterDataContainer characterDataContainer;

	@Mock
	private Character character;

	@Mock
	private ComicResponse response;

	@Mock
	private ComicDataContainer dataContainer;

	@Mock
	private Comic comic;

	@Mock
	private Price comicPrice;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testGetAverageComicPricesByYear() {
		
		int characterId = 42;

		when(client.getAllCharacters(any())).thenReturn(characterResponse);
		when(characterResponse.getResponseStatus()).thenReturn(Status.OK);
		when(characterResponse.getDataContainer()).thenReturn(characterDataContainer);
		when(characterDataContainer.getCount()).thenReturn(Integer.valueOf(1));
		when(characterDataContainer.getResults()).thenReturn(Collections.singletonList(character));
		when(character.getId()).thenReturn(characterId);
		
		when(client.getCharacterComics(eq(characterId), any())).thenReturn(response);
		when(response.getDataContainer()).thenReturn(dataContainer);
		when(response.getResponseStatus()).thenReturn(Status.OK);
		when(dataContainer.getResults()).thenReturn(Collections.singletonList(comic));
		when(dataContainer.getCount()).thenReturn(1, 0);
		when(dataContainer.getLimit()).thenReturn(20);

		when(comic.getPrices()).thenReturn(Collections.singletonList(comicPrice));
		when(comicPrice.getPrice()).thenReturn(42d);

		int yearHorizon = 5;
		// First call uses the client to retrieve data
		subject.getAverageComicPriceByYear("Spider Man", yearHorizon);
		// Second call retrieves, mostly, cached data
		subject.getAverageComicPriceByYear("Spider Man", yearHorizon + 1);
		
		ComicParameters.Builder expectedParameters = ComicParameters.builder().format(ComicFormat.COMIC);
		
		LocalDate now = LocalDate.now();
		for (int years = 0; years < (yearHorizon + 1); years++)
		{
			ComicParameters parameters = expectedParameters
					.dateRange(
							now.minusYears(years+1), 
							now.minusYears(years))
					.build();
			
			verify(client).getCharacterComics(eq(characterId), eq(parameters));
		}
	}

	@Test
	public void testInvalidCharacterName() {

		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage(allOf(containsString("character"), containsString("name")));

		subject.getAverageComicPriceByYear("", 5);
	}

	@Test
	public void testInvalidYearHorizon() {

		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage(allOf(containsString("year"), containsString("horizon")));

		subject.getAverageComicPriceByYear("Spider man", -1);
	}
}
