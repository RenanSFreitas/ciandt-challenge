package com.ciandt.marvel.api.client;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ciandt.marvel.api.model.character.CharacterResponse;
import com.ciandt.marvel.api.parameter.character.CharacterParameters;
import com.ciandt.marvel.api.parameter.comic.ComicFormat;
import com.ciandt.marvel.api.parameter.comic.ComicParameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ClientBuilder.class)
public class MarvelClientImplTest {

	@InjectMocks
	private MarvelClientImpl subject;

	@Mock
	private Client client;

	@Mock
	private WebTarget webTarget;

	@Mock
	private Invocation.Builder builder;

	
	@Before
	public void before() {
		commonStubbing();
	}

	@Test
	public void testGetCharacter() {

		subject.getCharacter(123);

		verify(client).target(eq("http://gateway.marvel.com/v1/public"));
		verify(webTarget).path("characters/123");
		verify(builder).get(eq(CharacterResponse.class));
	}

	@Test
	public void testGetAllCharacters() {

		CharacterParameters parameters = CharacterParameters.builder()
				.limit(123)
				.nameStartsWith("Spider")
				.build();

		subject.getAllCharacters(parameters);
		
		verify(client).target(eq("http://gateway.marvel.com/v1/public"));
		verify(webTarget).path("characters");
		verify(webTarget).queryParam(eq("limit"), eq(123));
		verify(webTarget).queryParam(eq("nameStartsWith"), eq("Spider"));
		verify(builder).get(eq(CharacterResponse.class));
	}
	
	@Test
	public void testGetCharacterComics()
	{
		LocalDate now = LocalDate.now();
		ComicParameters parameters = ComicParameters.builder()
				.limit(123)
				.format(ComicFormat.GRAPHIC_NOVEL)
				.dateRange(now.minusYears(2), now)
				.build();

		subject.getCharacterComics(321, parameters);
	}
	
	@SuppressWarnings("unchecked")
	private void commonStubbing() {
		PowerMockito.mockStatic(ClientBuilder.class);
		when(ClientBuilder.newClient()).thenReturn(client);
		when(client.target(anyString())).thenReturn(webTarget);
		when(webTarget.path(anyString())).thenReturn(webTarget);
		when(webTarget.queryParam(anyString(), any())).thenReturn(webTarget);
		when(webTarget.request(eq(MediaType.APPLICATION_JSON))).thenReturn(builder);
		when(builder.header(anyString(), any())).thenReturn(builder);
		when(builder.get(any(Class.class))).thenReturn(null);
	}
}
