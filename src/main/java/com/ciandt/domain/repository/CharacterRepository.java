package com.ciandt.domain.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ciandt.domain.Character;

public interface CharacterRepository extends JpaRepository<Character, Long> {

	@EntityGraph(value = "Character.enemies", type = EntityGraphType.LOAD)
	Character getByMarvelId(Integer marvelId);

	Character findByMarvelId(Integer marvelId);

	@Modifying
	@Transactional
	@Query("DELETE FROM Character c WHERE c.marvelId = :marvelId")
	void deleteByMarvelId(@Param("marvelId") Integer marvelId);
}
