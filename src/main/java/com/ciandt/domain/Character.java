package com.ciandt.domain;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;

@Entity
@NamedEntityGraph(name = "Character.enemies", attributeNodes = @NamedAttributeNode("enemies"))
public class Character {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(nullable=false)
	private String characterName;
	
	@Column(nullable = false, unique = true)
	private Integer marvelId;

	@ManyToMany
	private Set<Character> enemies = new HashSet<>();

	protected Character() {
	}

	public Character(Integer marvelId, String characterName) {
		this.marvelId = checkNotNull(marvelId);
		this.characterName = characterName;
	}

	public Integer getId() {
		return id;
	}

	public Integer getMarvelId() {
		return marvelId;
	}

	public void addEnemy(Character character) {
		checkNotNull(character);
		enemies.add(character);
	}

	public void removeEnemy(Character character) {
		enemies.remove(character);
	}

	public Set<Character> getEnemies() {
		return ImmutableSet.copyOf(enemies);
	}
	
	public String getCharacterName() {
		return characterName;
	}

	@Override
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || !getClass().equals(object.getClass())) {
			return false;
		}
		Character other = (Character) object;
		return Objects.equals(getMarvelId(), other.getMarvelId())
				&& Objects.equals(getCharacterName(), other.getCharacterName())
				&& Objects.equals(getEnemies(), other.getEnemies());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getMarvelId(), getCharacterName(), getEnemies());
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("characterName", characterName)
				.add("marvelId", marvelId)
				.add("enemies", getEnemies())
				.toString();
	}


}
