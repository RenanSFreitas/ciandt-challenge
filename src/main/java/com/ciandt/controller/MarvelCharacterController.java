package com.ciandt.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ciandt.domain.Character;
import com.ciandt.domain.repository.CharacterRepository;
import com.ciandt.marvel.api.Marvel;
import com.google.common.collect.ImmutableMap;

@RestController
public class MarvelCharacterController {

	@Autowired
	private Marvel marvel;
	@Autowired
	private CharacterRepository characterRepository;
	
	@RequestMapping(value = "/character/{characterMarvelId}/name", method = RequestMethod.GET)
	public String getCharacterName(@PathVariable("characterMarvelId") Integer characterMarvelId) 
	{
		return marvel.getCharacterName(characterMarvelId);
	}
	
	@RequestMapping(value = "/character", method = RequestMethod.GET)
	public List<Character> getCharacters()
	{
		return characterRepository.findAll();
	}
	
	@RequestMapping(value = "/character/{characterMarvelId}", method = RequestMethod.GET)
	public Set<String> getCharacterEnemies(
			@PathVariable("characterId") Integer characterMarvelId)
	{
		Character character = characterRepository.getByMarvelId(characterMarvelId);
		if(character == null)
		{
			return Collections.emptySet();
		}

		return character.getEnemies()
				.stream()
				.map(Character::getCharacterName)
				.sorted()
				.collect(Collectors.toSet());
	}

	@RequestMapping(value = "/character/{characterMarvelId}", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> saveCharacter(
			@PathVariable("characterMarvelId") Integer characterMarvelId) 
	{
		Character character = characterRepository.findByMarvelId(characterMarvelId);
		if(character==null) {
			character = new Character(characterMarvelId, marvel.getCharacterName(characterMarvelId));
			character = characterRepository.save(character);
		}

		return new ResponseEntity<>(ImmutableMap.<String, Object>builder()
				.put("characterMarvelId", characterMarvelId)
				.put("characterName", character.getCharacterName())
				.build(), HttpStatus.OK);
	}

	@RequestMapping(value = "/character/{characterMarvelId}/enemy/{enemyMarvelId}", method = RequestMethod.PUT)
	public Character saveCharacterEnemy(
			@PathVariable("characterMarvelId") Integer characterMarvelId,
			@PathVariable("enemyMarvelId") Integer enemyMarvelId) {
		
		Character character = characterRepository.getByMarvelId(characterMarvelId);
		if(character == null)
		{
			throw new IllegalArgumentException("Entity not found: " + characterMarvelId);
		}
		
		Character enemy = characterRepository.findByMarvelId(enemyMarvelId);
		
		if(enemy == null)
		{
			throw new IllegalArgumentException("Entity not found: " + enemyMarvelId);
		}
		
		character.addEnemy(enemy);
		character = characterRepository.save(character);
		return character;
	}

	@RequestMapping(value = "/character/{characterMarvelId}", method = RequestMethod.DELETE)
	public ResponseEntity<Integer> deleteCharacter(@PathVariable("characterMarvelId") Integer characterMarvelId) {

		characterRepository.deleteByMarvelId(characterMarvelId);
		return new ResponseEntity<>(characterMarvelId, HttpStatus.OK);
	}
}
