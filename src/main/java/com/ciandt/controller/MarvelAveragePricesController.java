package com.ciandt.controller;

import java.text.DecimalFormat;
import java.util.Map;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ciandt.marvel.api.Marvel;
import com.google.gson.Gson;

import jersey.repackaged.com.google.common.collect.Maps;

@RestController
public class MarvelAveragePricesController {

	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.0000");
	private static final Gson GSON = new Gson();

	@Autowired
	private Marvel marvel;

	@RequestMapping(value = "/averagePrices", method = RequestMethod.GET)
	public ResponseEntity<String> averagePrices(
			@QueryParam("heroName") String heroName, 
			@QueryParam("yearHorizon") int yearHorizon) 
	{
		MarvelControllerResponse<Map<Integer, String>> response = new MarvelControllerResponse<>();

		HttpStatus status = HttpStatus.OK;

		try {

			response.setCharacterId(marvel.getCharacterId(heroName));

			response.setContent(getAverageComicPriceByYear(heroName, yearHorizon));

		} catch (IllegalArgumentException e) {

			response.setErrorMessage(e.getMessage());
			status = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {

			if (e.getCause() instanceof IllegalArgumentException) {
				status = HttpStatus.BAD_REQUEST;
				response.setErrorMessage(e.getCause().getMessage());
			} else {
				status = HttpStatus.INTERNAL_SERVER_ERROR;
				response.setErrorMessage(e.getMessage());
			}
		}

		return new ResponseEntity<>(GSON.toJson(response), status);
	}

	private Map<Integer, String> getAverageComicPriceByYear(String heroName, int yearHorizon) {
		Map<Integer, String> averageComicPriceByYear = 
				Maps.transformValues(
						marvel.getAverageComicPriceByYear(heroName, yearHorizon), 
						average -> DECIMAL_FORMAT.format(average));
		return averageComicPriceByYear;
	}

}
