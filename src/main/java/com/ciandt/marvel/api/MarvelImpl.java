package com.ciandt.marvel.api;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.stream.Collectors.toSet;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ciandt.marvel.api.client.MarvelClient;
import com.ciandt.marvel.api.model.MarvelDataContainer;
import com.ciandt.marvel.api.model.MarvelResponse;
import com.ciandt.marvel.api.model.Price;
import com.ciandt.marvel.api.model.character.Character;
import com.ciandt.marvel.api.model.character.CharacterDataContainer;
import com.ciandt.marvel.api.model.character.CharacterResponse;
import com.ciandt.marvel.api.model.comic.ComicDataContainer;
import com.ciandt.marvel.api.model.comic.ComicResponse;
import com.ciandt.marvel.api.parameter.character.CharacterParameters;
import com.ciandt.marvel.api.parameter.comic.ComicFormat;
import com.ciandt.marvel.api.parameter.comic.ComicParameters;
import com.ciandt.marvel.api.utils.Pair;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.math.DoubleMath;

import jersey.repackaged.com.google.common.base.Throwables;

/**
 * Standard implementation for the {@link Marvel} facade interface.
 * <p>
 * Uses caching of data with Marvel recommended expiration settings.
 */
@Component
public final class MarvelImpl implements Marvel {

	static enum MarvelOperation {

		CHARACTER_ID {
			@Override
			Object run(MarvelImpl marvel, List<?> params) {
				return marvel.doGetCharacterId((String) params.get(0));
			}
		},
		AVERAGE_COMIC_PRICE_BY_YEAR {
			@Override
			Object run(MarvelImpl marvel, List<?> params) {
				return marvel.doGetAverageComicPriceByYear(
						(String) params.get(0), 
						(Integer) params.get(1));
			}
		}, 
		CHARACTER_NAME {
			@Override
			Object run(MarvelImpl marvel, List<?> params) {
				return marvel.doGetCharacterName((Integer) params.get(0));
			}
		};

		abstract Object run(MarvelImpl marvel, List<?> params);
	}

	private final MarvelClient client;

	private final CacheLoader<Pair<MarvelOperation, List<?>>, Object> cacheLoader = new CacheLoader<Pair<MarvelOperation, List<?>>, Object>() {
		@Override
		public Object load(Pair<MarvelOperation, List<?>> key) throws Exception {
			MarvelOperation operation = key.getFirst();
			List<?> parameters = key.getSecond();
			return operation.run(MarvelImpl.this, parameters);
		};

	};
	private final LoadingCache<Pair<MarvelOperation, List<?>>, Object> cache = CacheBuilder.newBuilder()
			.maximumSize(1_000)
			.expireAfterWrite(24, TimeUnit.HOURS)
			.build(cacheLoader);

	@Autowired
	public MarvelImpl(MarvelClient client) {
		this.client = client;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, Double> getAverageComicPriceByYear(String characterName, int yearHorizon) {
		checkArgument(!isNullOrEmpty(characterName), "Invalid input: character name is null or empty");
		checkArgument(yearHorizon >= 1, "Invalid input: year horizon should be greater or equal 1");

		Pair<MarvelOperation, List<?>> cacheKey = Pair.of(
				MarvelOperation.AVERAGE_COMIC_PRICE_BY_YEAR,
				ImmutableList.of(characterName, yearHorizon, "result"));

		Object value = cache.getIfPresent(cacheKey);

		if (value == null) {
			loadIntermediateAverageComicPriceByYear(cacheKey, characterName, yearHorizon);
		}

		return (Map<Integer, Double>) cache.getUnchecked(cacheKey);
	}

	@SuppressWarnings("unchecked")
	private void loadIntermediateAverageComicPriceByYear(Pair<MarvelOperation, List<?>> resultCacheKey,
			String characterName, int yearHorizon) {
		
		Map<Integer, Double> result = new TreeMap<>();
		for (int yearDisplacement = 1; yearDisplacement <= yearHorizon; yearDisplacement++)
		{
			Pair<MarvelOperation, List<?>> cacheKey = Pair.of(
					MarvelOperation.AVERAGE_COMIC_PRICE_BY_YEAR,
					ImmutableList.of(characterName, yearDisplacement, "intermediate"));

			try {
				result.putAll((Map<Integer, Double>) cache.get(cacheKey));
			} catch (ExecutionException e) {
				throw Throwables.propagate(e);
			}
		}

		cache.put(resultCacheKey, result);
	}

	private Map<Integer, Double> doGetAverageComicPriceByYear(String characterName, int yearDisplacement) {

		Integer characterId = getCharacterId(characterName);

		Set<Double> prices = new HashSet<>();

		int year = collectYearPrices(characterId, yearDisplacement, prices);

		return Collections.singletonMap(Integer.valueOf(year), DoubleMath.mean(prices));
	}

	private int collectYearPrices(Integer characterId, int yearDisplacement, Set<Double> prices) {

		LocalDate now = LocalDate.now();
		int year = now.getYear() - (yearDisplacement - 1);
		LocalDate to = LocalDate.of(year, now.getMonth(), now.getDayOfMonth());
		LocalDate from = LocalDate.of(year - 1, now.getMonth(), now.getDayOfMonth());

		ComicParameters.Builder parametersBuilder = ComicParameters.builder()
				.format(ComicFormat.COMIC)
				.dateRange(from, to);

		ComicResponse comicResponse = client.getCharacterComics(characterId, parametersBuilder.build());
		checkMarvelResponseStatus(comicResponse);

		ComicDataContainer comicsContainer = comicResponse.getDataContainer();

		prices.addAll(collectComicsPrices(comicsContainer));

		int offset = comicsContainer.getCount().intValue();
		while (shouldContinueSendingRequests(comicsContainer)) {
			ComicParameters parameters = parametersBuilder.offset(offset).build();

			comicResponse = client.getCharacterComics(characterId, parameters);
			checkMarvelResponseStatus(comicResponse);

			comicsContainer = comicResponse.getDataContainer();
			prices.addAll(collectComicsPrices(comicsContainer));
			offset += comicsContainer.getCount().intValue();
		}
		return year;
	}

	private boolean shouldContinueSendingRequests(ComicDataContainer comicsContainer) {
		return comicsContainer.getCount().intValue() == comicsContainer.getLimit().intValue();
	}

	@Override
	public Integer getCharacterId(String characterName) {

		try {
			return (Integer) cache.get(Pair.of(MarvelOperation.CHARACTER_ID, Collections.singletonList(characterName)));
		} catch (ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getCharacterName(Integer characterId) {
		try {
			return (String) cache.get(Pair.of(
					MarvelOperation.CHARACTER_NAME, 
					Collections.singletonList(characterId)));
		} catch (ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected String doGetCharacterName(Integer characterId) {
		
		CharacterResponse response = client.getCharacter(characterId.intValue());
		checkMarvelResponseStatus(response);
		return Iterables.getOnlyElement(response.getDataContainer().getResults()).getName();
	}

	private Integer doGetCharacterId(String characterName) {

		CharacterResponse characterResponse = client
				.getAllCharacters(CharacterParameters.builder().name(characterName).build());

		checkMarvelResponseStatus(characterResponse);

		CharacterDataContainer dataContainer = characterResponse.getDataContainer();
		int characterCount = dataContainer.getCount().intValue();
		checkArgument(characterCount == 1, String.format("Character name %s was expected to return one result, but returned %d.", 
				characterName, characterCount));

		Character character = Iterables.getOnlyElement(dataContainer.getResults());
		Integer characterId = character.getId();
		return characterId;
	}

	private <T extends MarvelDataContainer<?>> void checkMarvelResponseStatus(MarvelResponse<T> characterResponse) {
		Status responseStatus = characterResponse.getResponseStatus();
		checkArgument(Response.Status.OK.equals(responseStatus), "Unexpected response status " + responseStatus);
	}

	private Set<Double> collectComicsPrices(ComicDataContainer comicsContainer) {
		return comicsContainer.getResults()
				.stream()
				.flatMap(s -> s.getPrices().stream().map(Price::getPrice))
				.collect(toSet());
	}
}
