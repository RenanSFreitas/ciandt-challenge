package com.ciandt.marvel.api.deserializer;

import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public final class HttpResponseStatusDeserializer extends JsonDeserializer<Response.Status> {

	@Override
	public Status deserialize(JsonParser parser, DeserializationContext context)
			throws IOException, JsonProcessingException {

		return Response.Status.fromStatusCode(parser.getValueAsInt());
	}

}
