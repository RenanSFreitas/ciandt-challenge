package com.ciandt.marvel.api.client;

import com.ciandt.marvel.api.model.character.CharacterResponse;
import com.ciandt.marvel.api.model.comic.ComicResponse;
import com.ciandt.marvel.api.parameter.character.CharacterParameters;
import com.ciandt.marvel.api.parameter.comic.ComicParameters;

public interface MarvelClient {

	CharacterResponse getCharacter(int characterId);

	CharacterResponse getAllCharacters();

	CharacterResponse getAllCharacters(CharacterParameters parameters);

	ComicResponse getCharacterComics(int characterId);

	ComicResponse getCharacterComics(int characterId, ComicParameters parameters);

}