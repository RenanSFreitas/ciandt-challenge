package com.ciandt.marvel.api.client;

import java.util.Map.Entry;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import com.ciandt.marvel.api.model.character.CharacterResponse;
import com.ciandt.marvel.api.model.comic.ComicResponse;
import com.ciandt.marvel.api.parameter.MarvelParameters;
import com.ciandt.marvel.api.parameter.character.CharacterParameters;
import com.ciandt.marvel.api.parameter.comic.ComicParameters;
import com.google.common.base.Joiner;

@Component
public final class MarvelClientImpl implements MarvelClient {

	private static final String COMICS = "comics";
	private static final String CHARACTERS = "characters";
	private static final String MARVEL_GATEWAY = "http://gateway.marvel.com";
	private static final String BASE_URI = MARVEL_GATEWAY + "/v1/public";

	private static final String MARVEL_API_PUBLIC_KEY = System.getProperty("marvel.api.public.key", "4bd1ed3322b194d7520ad7d56df8d3a5");
	private static final String MARVEL_API_PRIVATE_KEY = System.getProperty("marvel.api.private.key", "3302abe94673be5adcd45cbecbf4dc68f2163397");
	
	@Override
	public CharacterResponse getCharacter(int characterId) {

		return newMarvelApiInvocation(CharacterResponse.class, CHARACTERS, characterId);
	}
	
	@Override
	public CharacterResponse getAllCharacters() {

		return newMarvelApiInvocation(CharacterResponse.class, CHARACTERS);
	}

	@Override
	public CharacterResponse getAllCharacters(CharacterParameters parameters) {

		return newMarvelApiInvocation(CharacterResponse.class, parameters, CHARACTERS);
	}

	@Override
	public ComicResponse getCharacterComics(int characterId) {

		return newMarvelApiInvocation(ComicResponse.class, CHARACTERS, characterId, COMICS);
	}

	@Override
	public ComicResponse getCharacterComics(int characterId, ComicParameters parameters) {

		return newMarvelApiInvocation(ComicResponse.class, parameters, CHARACTERS, characterId, COMICS);
	}

	private <T> T newMarvelApiInvocation(Class<T> responseType, Object... path) {
		return newMarvelApiInvocation(responseType, MarvelParameters.EMPTY, path);
	}

	private <T> T newMarvelApiInvocation(Class<T> responseType, MarvelParameters parameters, Object... path) {

		long ts = System.currentTimeMillis();

		WebTarget target = ClientBuilder.newClient().target(BASE_URI)
				.path(Joiner.on('/').join(path))
				.queryParam("apikey", MARVEL_API_PUBLIC_KEY)
				.queryParam("ts", ts)
				.queryParam("hash", DigestUtils.md5DigestAsHex((ts + MARVEL_API_PRIVATE_KEY + MARVEL_API_PUBLIC_KEY).getBytes()));

		for (Entry<String, Object> entry : parameters.asMap().entrySet()) {
			target = target.queryParam(entry.getKey(), entry.getValue());
		}

		return target.request(MediaType.APPLICATION_JSON).header("Accept", "*/*").get(responseType);
	}
}
