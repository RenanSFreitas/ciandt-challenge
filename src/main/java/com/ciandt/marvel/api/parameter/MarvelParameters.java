package com.ciandt.marvel.api.parameter;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;

public abstract class MarvelParameters {

	public static MarvelParameters EMPTY = new MarvelParameters(Collections.emptyMap()) {
		// Empty implementation
	};

	private final Map<String, Object> parameters;

	public MarvelParameters() {
		this(new HashMap<>());
	}

	private MarvelParameters(Map<String, Object> parametersMap) {
		this.parameters = parametersMap;
	}

	public final void setLimit(int limit) {
		parameters.put("limit", Integer.valueOf(limit));
	}

	public final void setOffset(int offset) {
		parameters.put("offset", Integer.valueOf(offset));
	}

	public final void removeParameter(String parameterName) {
		parameters.remove(parameterName);
	}

	protected final void setParameter(String key, Object value) {
		checkArgument(!isNullOrEmpty(key));
		checkNotNull(value);
		parameters.put(key, value);
	}

	public Map<String, Object> asMap() {
		return ImmutableMap.copyOf(parameters);
	};

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if ((other == null) || !getClass().equals(other.getClass())) {
			return false;
		}
		MarvelParameters castOther = (MarvelParameters) other;
		return Objects.equals(parameters, castOther.parameters);
	}

	@Override
	public int hashCode() {
		return Objects.hash(parameters);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("parameters", parameters).toString();
	}

	public abstract static class Builder<T extends Builder<T>> {

		protected Integer limit;
		protected Integer offset;

		public abstract T limit(int limit);

		public abstract T offset(int offset);

		protected void build(MarvelParameters parameters) {
			if (limit != null) {
				checkArgument(limit.intValue() > 0, "Invalid limit, should be greater than zero");
				parameters.setLimit(limit.intValue());
			}

			if (offset != null) {
				checkArgument(offset.intValue() > 0, "Invalid offset, should be greater than zero");
				parameters.setOffset(offset.intValue());
			}
		}
	}

}
