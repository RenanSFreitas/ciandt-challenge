package com.ciandt.marvel.api.parameter.comic;

public enum ComicFormat {

	COMIC,
	MAGAZINE,
	GRAPHIC_NOVEL,
	DIGITAL_COMIC,
	DIGEST,
	INFINITE_COMIC;
	
	public String toParameterValue() {
		return toString().toLowerCase().replaceAll("_", " ");
	}
}
