package com.ciandt.marvel.api.parameter.comic;

import static com.google.common.base.Preconditions.checkNotNull;
import static jersey.repackaged.com.google.common.base.Preconditions.checkState;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.ciandt.marvel.api.parameter.MarvelParameters;

public class ComicParameters extends MarvelParameters {

	private DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("YYYY-MM-dd");

	public static Builder builder() {
		return new Builder();
	}

	public void setFormat(ComicFormat format) {
		setParameter("format", format.toParameterValue());
	}

	public void setDateRange(LocalDate from, LocalDate to) {
		
		StringBuilder builder = new StringBuilder();
		builder
			.append(DATE_FORMAT.format(from))
			.append(",")
			.append(DATE_FORMAT.format(to));
		setParameter("dateRange", builder.toString());
	}
	
	public static class Builder extends MarvelParameters.Builder<Builder> {
		
		private ComicFormat format;

		private LocalDate from;
		private LocalDate to;

		@Override
		public Builder limit(int limit) {
			this.limit = Integer.valueOf(limit);
			return this;
		}

		@Override
		public Builder offset(int offset) {
			this.offset = Integer.valueOf(offset);
			return this;
		}

		public Builder format(ComicFormat format) {
			this.format = checkNotNull(format);
			return this;
		}

		public Builder dateRange(LocalDate from, LocalDate to) {
			this.from = checkNotNull(from);
			this.to = checkNotNull(to);
			return this;
		}

		public ComicParameters build() {
			ComicParameters parameters = new ComicParameters();
			build(parameters);
			if (format != null) {
				parameters.setFormat(format);
			}

			if (from != null) {
				checkState(from.isBefore(to), "When setting date range, from Date must be before to Date");
				parameters.setDateRange(from, to);
			}

			return parameters;
		}
	}
}
