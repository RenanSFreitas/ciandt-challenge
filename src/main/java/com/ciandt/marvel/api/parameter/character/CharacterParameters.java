package com.ciandt.marvel.api.parameter.character;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import com.ciandt.marvel.api.parameter.MarvelParameters;

public final class CharacterParameters extends MarvelParameters {

	public static Builder builder() {
		return new Builder();
	}

	public void setName(String name) {
		setParameter("name", name);
	}

	public void setNameStartsWith(String name) {
		setParameter("nameStartsWith", name);
	}

	public static class Builder extends MarvelParameters.Builder<CharacterParameters.Builder> {

		private String nameStartsWith;
		private String name;

		private Builder() {
			// private constructor
		}

		@Override
		public Builder limit(int limit) {
			this.limit = Integer.valueOf(limit);
			return this;
		}

		@Override
		public Builder offset(int offset) {
			this.offset = Integer.valueOf(offset);
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder nameStartsWith(String name) {
			this.nameStartsWith = name;
			return this;
		}

		public CharacterParameters build() {

			CharacterParameters parameters = new CharacterParameters();

			build(parameters);

			if (name != null) {
				checkArgument(!isNullOrEmpty(name));
				parameters.setName(name);
			}

			if (nameStartsWith != null) {
				checkArgument(!isNullOrEmpty(nameStartsWith));
				parameters.setNameStartsWith(nameStartsWith);
			}

			return parameters;
		}
	}
}
