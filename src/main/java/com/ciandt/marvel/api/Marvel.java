package com.ciandt.marvel.api;

import java.util.Map;

/**
 * Facade for interacting with the Marvel API.
 */
public interface Marvel {

	/**
	 * Returns the arithmetic average of comic prices grouped by year, for the
	 * given <code>characterName</code> and <code>yearHorizon</code>. The
	 * iteration order of the returned Map is guaranteed to be the same as the
	 * years natural ordering.
	 * 
	 * @param characterName
	 *            the name of the character whose comics are to be retrieved
	 * @param yearHorizon
	 *            the amount of years in the past for which the averages should
	 *            be calculated, i.e., <code>yearHorizon = 3</code> implies on
	 *            calculating the averages for the present year and the two
	 *            years prior to it
	 * @return the arithmetic average of comic prices grouped by year, i.e., a
	 *         Map whose keys are years and values are the averages
	 * @throws IllegalArgumentException
	 *             when no single Marvel character can be found for the give
	 *             <code>characterName</code>
	 */
	Map<Integer, Double> getAverageComicPriceByYear(String characterName, int yearHorizon);

	/**
	 * Retrieves the character id for the single character found with the given
	 * <code>characterName</code>.
	 * 
	 * @param characterName
	 *            the character name to search for. Must be a non-blank String.
	 * @return character id for the single character found
	 * @throws IllegalArgumentException
	 *             when no single Marvel character can be found for the give
	 *             <code>characterName</code>
	 */
	Integer getCharacterId(String characterName);

	/**
	 * Retrieves the character name for the character found with the given
	 * <code>characterId</code>.
	 * 
	 * @param characterId
	 *            the character id to search for
	 * @return character name of the character found
	 */
	String getCharacterName(Integer characterId);
}