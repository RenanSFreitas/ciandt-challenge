package com.ciandt.marvel.api.utils;

import static jersey.repackaged.com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import com.google.common.base.MoreObjects;

public final class Pair<F, S> {

	private F first;
	private S second;

	public static <F, S> Pair<F, S> of(F first, S second)
	{
		return new Pair<>(first, second);
	}

	private Pair(F first, S second) {
		this.first = checkNotNull(first);
		this.second = checkNotNull(second);
	}

	public F getFirst() {
		return first;
	}

	public S getSecond() {
		return second;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}
		if ((object == null) || !getClass().equals(object.getClass())) {
			return false;
		}
		Pair<F, S> other = (Pair<F, S>) object;
		return Objects.equals(first, other.first) && Objects.equals(second, other.second);
	}

	@Override
	public int hashCode() {
		return Objects.hash(first, second);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("first", first)
				.add("second", second)
				.toString();
	}


}
