package com.ciandt.marvel.api.model;

import java.util.List;

public class TypableResourceSummaryContainer extends AbstractResourceSummaryContainer<TypableResourceSummary> {

	private List<TypableResourceSummary> items;

	@Override
	public List<TypableResourceSummary> getItems() {
		return items;
	}

	@Override
	public void setItems(List<TypableResourceSummary> items) {
		this.items = items;
	}
}
