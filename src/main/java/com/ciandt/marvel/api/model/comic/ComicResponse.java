package com.ciandt.marvel.api.model.comic;

import com.ciandt.marvel.api.model.MarvelResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

@JsonIgnoreProperties("status")
public class ComicResponse extends MarvelResponse<ComicDataContainer> {

	@JsonProperty("data")
	private ComicDataContainer dataContainer;

	@Override
	public ComicDataContainer getDataContainer() {
		return dataContainer;
	}

	@Override
	public void setDataContainer(ComicDataContainer dataContainer) {
		this.dataContainer = dataContainer;
	}

	@Override
	public String toString() {
		ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
		toString(toStringHelper);
		return toStringHelper.add("dataContainer", dataContainer).toString();
	}
}
