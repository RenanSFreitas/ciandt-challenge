package com.ciandt.marvel.api.model.comic;


import java.util.List;

import com.ciandt.marvel.api.model.Creators;
import com.ciandt.marvel.api.model.Dates;
import com.ciandt.marvel.api.model.Images;
import com.ciandt.marvel.api.model.MarvelUrl;
import com.ciandt.marvel.api.model.Price;
import com.ciandt.marvel.api.model.ResourceSummary;
import com.ciandt.marvel.api.model.ResourceSummaryContainer;
import com.ciandt.marvel.api.model.TextObject;
import com.ciandt.marvel.api.model.Thumbnail;
import com.ciandt.marvel.api.model.TypableResourceSummaryContainer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "variants", "collections", "collectedIssues", "digitalId", "diamondCode", "ean", "issn", "isbn",
		"upc", "isbn", "variantDescription" })
public class Comic {

	private ResourceSummary series;

	private ResourceSummaryContainer events;

	private String id;

	private String title;

	private List<Dates> dates;

	private String description;

	private String pageCount;

	private List<TextObject> textObjects;

	private List<MarvelUrl> urls;

	private String format;

	private String modified;

	private Creators creators;

	private String issueNumber;

	private TypableResourceSummaryContainer stories;

	private Thumbnail thumbnail;

	private String resourceURI;

	private List<Images> images;

	private List<String> collectedIssues;

	private List<Price> prices;

	private ResourceSummaryContainer characters;

	public ResourceSummary getSeries() {
		return series;
	}

	public void setSeries(ResourceSummary series) {
		this.series = series;
	}

	public ResourceSummaryContainer getEvents() {
		return events;
	}

	public void setEvents(ResourceSummaryContainer events) {
		this.events = events;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Dates> getDates() {
		return dates;
	}

	public void setDates(List<Dates> dates) {
		this.dates = dates;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPageCount() {
		return pageCount;
	}

	public void setPageCount(String pageCount) {
		this.pageCount = pageCount;
	}

	public List<TextObject> getTextObjects() {
		return textObjects;
	}

	public void setTextObjects(List<TextObject> textObjects) {
		this.textObjects = textObjects;
	}

	public List<MarvelUrl> getUrls() {
		return urls;
	}

	public void setUrls(List<MarvelUrl> urls) {
		this.urls = urls;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Creators getCreators() {
		return creators;
	}

	public void setCreators(Creators creators) {
		this.creators = creators;
	}

	public String getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(String issueNumber) {
		this.issueNumber = issueNumber;
	}

	public TypableResourceSummaryContainer getStories() {
		return stories;
	}

	public void setStories(TypableResourceSummaryContainer stories) {
		this.stories = stories;
	}

	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public List<Images> getImages() {
		return images;
	}

	public void setImages(List<Images> images) {
		this.images = images;
	}

	public List<String> getCollectedIssues() {
		return collectedIssues;
	}

	public void setCollectedIssues(List<String> collectedIssues) {
		this.collectedIssues = collectedIssues;
	}

	public List<Price> getPrices() {
		return prices;
	}

	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}

	public ResourceSummaryContainer getCharacters() {
		return characters;
	}

	public void setCharacters(ResourceSummaryContainer characters) {
		this.characters = characters;
	}
}
