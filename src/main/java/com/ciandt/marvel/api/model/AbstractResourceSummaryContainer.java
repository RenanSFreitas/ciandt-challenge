package com.ciandt.marvel.api.model;

import java.util.List;

public abstract class AbstractResourceSummaryContainer<T extends ResourceSummary> {

	private String collectionURI;

	private String available;

	private String returned;

	public abstract List<T> getItems();

	public abstract void setItems(List<T> items);

	public String getCollectionURI() {
		return collectionURI;
	}

	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getReturned() {
		return returned;
	}

	public void setReturned(String returned) {
		this.returned = returned;
	}
}
