package com.ciandt.marvel.api.model;

import java.util.List;

public class ResourceSummaryContainer extends AbstractResourceSummaryContainer<ResourceSummary> {

	private List<ResourceSummary> items;

	@Override
	public List<ResourceSummary> getItems() {
		return items;
	}

	@Override
	public void setItems(List<ResourceSummary> items) {
		this.items = items;
	}

}
