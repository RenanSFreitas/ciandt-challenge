package com.ciandt.marvel.api.model.character;

import java.util.Date;
import java.util.List;

import com.ciandt.marvel.api.model.MarvelUrl;
import com.ciandt.marvel.api.model.ResourceSummaryContainer;
import com.ciandt.marvel.api.model.Thumbnail;
import com.ciandt.marvel.api.model.TypableResourceSummaryContainer;

public class Character
{
	private Integer id;

	private ResourceSummaryContainer series;

    private TypableResourceSummaryContainer stories;

    private Thumbnail thumbnail;

    private String resourceURI;

	private List<MarvelUrl> urls;

	private ResourceSummaryContainer events;

    private String description;

    private String name;

	private ResourceSummaryContainer comics;

	private Date modified;

	public Integer getId()
    {
        return id;
    }

	public void setId(Integer id)
    {
        this.id = id;
    }

	public ResourceSummaryContainer getSeries()
    {
        return series;
    }

	public void setSeries(ResourceSummaryContainer series)
    {
        this.series = series;
    }

    public TypableResourceSummaryContainer getStories ()
    {
        return stories;
    }

    public void setStories (TypableResourceSummaryContainer stories)
    {
        this.stories = stories;
    }

    public Thumbnail getThumbnail ()
    {
        return thumbnail;
    }

    public void setThumbnail (Thumbnail thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public String getResourceURI ()
    {
        return resourceURI;
    }

    public void setResourceURI (String resourceURI)
    {
        this.resourceURI = resourceURI;
    }

	public List<MarvelUrl> getUrls()
    {
        return urls;
    }

	public void setUrls(List<MarvelUrl> urls)
    {
        this.urls = urls;
    }

	public ResourceSummaryContainer getEvents()
    {
        return events;
    }

	public void setEvents(ResourceSummaryContainer events)
    {
        this.events = events;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

	public ResourceSummaryContainer getComics()
    {
        return comics;
    }

	public void setComics(ResourceSummaryContainer comics)
    {
        this.comics = comics;
    }

	public Date getModified()
    {
        return modified;
    }

	public void setModified(Date modified)
    {
        this.modified = modified;
    }
}
	