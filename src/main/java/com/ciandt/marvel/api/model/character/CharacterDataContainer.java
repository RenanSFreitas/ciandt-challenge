package com.ciandt.marvel.api.model.character;

import java.util.List;

import com.ciandt.marvel.api.model.MarvelDataContainer;

public class CharacterDataContainer extends MarvelDataContainer<Character>
{
	private List<Character> results;

	@Override
	public List<Character> getResults() {
		return results;
	}

	@Override
	public void setResults(List<Character> results) {
		this.results = results;
	}

}