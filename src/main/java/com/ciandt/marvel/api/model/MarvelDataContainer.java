package com.ciandt.marvel.api.model;

import java.util.List;

public abstract class MarvelDataContainer<T> {

	private Integer total;

	private Integer limit;

	private Integer count;

	private Integer offset;

	public abstract List<T> getResults();

	public abstract void setResults(List<T> results);

	public final Integer getTotal() {
		return total;
	}

	public final void setTotal(Integer total) {
		this.total = total;
	}

	public final Integer getLimit() {
		return limit;
	}

	public final void setLimit(Integer limit) {
		this.limit = limit;
	}

	public final Integer getCount() {
		return count;
	}

	public final void setCount(Integer count) {
		this.count = count;
	}

	public final Integer getOffset() {
		return offset;
	}

	public final void setOffset(Integer offset) {
		this.offset = offset;
	}

}
