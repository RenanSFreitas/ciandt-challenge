package com.ciandt.marvel.api.model.character;

import com.ciandt.marvel.api.model.MarvelResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

@JsonIgnoreProperties("status")
public class CharacterResponse extends MarvelResponse<CharacterDataContainer> {
	
	@JsonProperty("data")
	private CharacterDataContainer dataContainer;

	@Override
	public CharacterDataContainer getDataContainer() {
		return dataContainer;
	}

	@Override
	public void setDataContainer(CharacterDataContainer dataContainer) {
		this.dataContainer = dataContainer;
	}

	@Override
	public String toString() {
		ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
		toString(toStringHelper);
		return toStringHelper.add("dataContainer", dataContainer).toString();
	}
}