package com.ciandt.marvel.api.model;

import javax.ws.rs.core.Response;

import com.ciandt.marvel.api.deserializer.HttpResponseStatusDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects.ToStringHelper;

public abstract class MarvelResponse<T extends MarvelDataContainer<?>> {

	private String attributionText;

	private String etag;

	@JsonDeserialize(using = HttpResponseStatusDeserializer.class)
	@JsonProperty("code")
	private Response.Status responseStatus;

	private String attributionHTML;

	private String copyright;

	public abstract T getDataContainer();

	public abstract void setDataContainer(T dataContainer);

	public final String getAttributionText() {
		return attributionText;
	}

	public final void setAttributionText(String attributionText) {
		this.attributionText = attributionText;
	}

	public final String getEtag() {
		return etag;
	}

	public final void setEtag(String etag) {
		this.etag = etag;
	}

	public final String getCopyright() {
		return copyright;
	}

	public final void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public final Response.Status getResponseStatus() {
		return responseStatus;
	}

	public final void setResponseStatus(Response.Status responseStatus) {
		this.responseStatus = responseStatus;
	}

	public final String getAttributionHTML() {
		return attributionHTML;
	}

	public final void setAttributionHTML(String attributionHTML) {
		this.attributionHTML = attributionHTML;
	}

	public final void toString(ToStringHelper toStringHelper) {
		toStringHelper
				.add("attributionText", attributionText)
				.add("etag", etag)
				.add("responseStatus", responseStatus)
				.add("attributionHTML", attributionHTML)
				.add("copyright", copyright);
	}
	
}
