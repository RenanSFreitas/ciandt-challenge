package com.ciandt.marvel.api.model.comic;

import java.util.List;

import com.ciandt.marvel.api.model.MarvelDataContainer;

public class ComicDataContainer extends MarvelDataContainer<Comic> 
{
	private List<Comic> results;

	@Override
	public List<Comic> getResults() {
		return results;
	}

	@Override
	public void setResults(List<Comic> results) {
		this.results = results;
	}

}
