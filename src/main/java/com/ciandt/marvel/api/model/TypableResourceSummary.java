package com.ciandt.marvel.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;

public class TypableResourceSummary extends ResourceSummary {

	private String type;

	@JsonCreator
	public static TypableResourceSummary of(
			@JsonProperty("resourceURI") String resourceURI, 
			@JsonProperty("name") String name, 
			@JsonProperty(value="type", required=false) String type, 
			@JsonProperty(value="role", required=false) String role) 
	{
		TypableResourceSummary summary = new TypableResourceSummary();
		summary.setName(name);
		summary.setResourceURI(resourceURI);

		String typeValue;

		if (type != null) {
			typeValue = type;
			Preconditions.checkArgument(role == null);
		} else if (role != null) {
			typeValue = role;
			Preconditions.checkArgument(type == null);
		} else {
			throw new IllegalArgumentException();
		}
		summary.setType(typeValue);

		return summary;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
