package com.ciandt.marvel.api.model.comic;

import com.ciandt.marvel.api.model.ResourceSummary;

public class Comics {
	private ResourceSummary[] items;

	private String collectionURI;

	private String available;

	private String returned;

	public ResourceSummary[] getItems() {
		return items;
	}

	public void setItems(ResourceSummary[] items) {
		this.items = items;
	}

	public String getCollectionURI() {
		return collectionURI;
	}

	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getReturned() {
		return returned;
	}

	public void setReturned(String returned) {
		this.returned = returned;
	}
}