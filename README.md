# README #

CI&T Challenge. Uses Marvel API to provide a REST service for averaging the comic prices of a certain character over the years and also provides REST services to create/update/delete records of Marvel enemies per Marvel character.

Implementation initially made on top of https://bitbucket.org/andremp_cit/hands-on

### Notes ###

* Instead of Java 7 as the hands-on project, Java 8 was used
* Additional dependencies were added to the pom.xml file (Guava, Mockito, Powermock, Spring-Data, etc)
* In a real environment, the package com.ciandt.marvel.api should be a project on its own